%{
package custom

func setResult(l yyLexer, v Result) {
  l.(*lex).result = v
}
%}

%union{
val interface{}
}
%token LexError
%token <val> FUNC ARG

%start main

%%

main: FUNC '(' ARG ')'
 {
    setResult(yylex, Result{Name: $1,Value:$3})
    res($3)
 }


