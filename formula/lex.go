package formula

import (
	"bytes"
	"errors"
	"unicode"
)

//go:generate go run golang.org/x/tools/cmd/goyacc -l -o parser.go parser.y

// Result is the type of the parser result
type Result bool

type lex struct {
	input   []byte
	result  bool
	err    error
	pos    int
}

func newLex(input []byte) *lex {
	return &lex{
		input: input,
	}
}

// Parse parses the input and returs the result.
func Parse(input []byte) (bool, error) {
	l := newLex(input)
	_ = yyParse(l)
	return l.result, l.err
}

// Lex satisfies yyLexer.
func (l *lex) Lex(lval *yySymType) int {
	return l.scanNormal(lval)
}

func (l *lex) scanNormal(lval *yySymType) int {
	for b := l.next(); b != 0; b = l.next() {
		switch {
		case unicode.IsSpace(rune(b)):
			continue
		case unicode.IsNumber(rune(b)):
			lval.num = int(rune(b))
			return NUMBER
		case unicode.IsLetter(rune(b)):
			l.backup()
			return l.scanString(lval)
		default:
			return int(b)
		}
	}
	return 0
}

func (l *lex) scanString(lval *yySymType) int {
	buf := bytes.NewBuffer(nil)
	for b := l.next(); b != 0; b = l.next() {
		switch b {
		case ' ':
			lval.str = buf.String()
			if buf.String() == "AND" {
				return AND
			}
			return OR
		default:
			buf.WriteByte(b)
		}
	}
	return LexError
}

// Error satisfies yyLexer.
func (l *lex) Error(s string) {
	l.err = errors.New(s)
}

func (l *lex) next() byte {
	if l.pos >= len(l.input) || l.pos == -1 {
		l.pos = -1
		return 0
	}
	l.pos++
	return l.input[l.pos-1]
}

func (l *lex) backup() {
	if l.pos == -1 {
		return
	}
	l.pos--
}

