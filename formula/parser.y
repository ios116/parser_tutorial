%{
package formula
func setResult(l yyLexer, v bool) {
  l.(*lex).result = v
}
%}

%union{
num int
str string
boolean bool
char rune
}

%token LexError
%token <num> NUMBER
%token <str> AND OR

%type <boolean> result expression

%start main

%%

main: result
 {
    setResult(yylex,$1)
 }

result: expression
{
 $$ = $1
}
|  result AND expression
{
 $$ = $1 && $3
}
| result OR expression
{
 $$ = $1 || $3
}

expression:
NUMBER '>' NUMBER
{
   $$ = $1 > $3
}
| NUMBER '<' NUMBER
{
   $$ = $1 < $3
}