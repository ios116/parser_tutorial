package formula

import (
	"testing"
)

func TestParser(t *testing.T) {
	str:= `6>5 AND 5>3 AND 9>6 AND 8>7 AND 7>6`

	r, err :=Parse([]byte(str))
	if err != nil {
		t.Fatal(err)
	}
	t.Log(r)
}
