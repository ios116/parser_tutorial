package newformula

import (
	"fmt"
)

type ResultActions struct {
	Value int
}

func TransactionAccountCounter(context *lex,arg1, arg2 string) CounterObj {
	return CounterObj{Value:20, Limit: 45}
}

func TransactionRule(context *lex, arg1 string) BusinessRule {

	return BusinessRule{Description:"Hello"}
}

func TransactionCustomerCustomAttribute(context *lex,arg1 string) []string {
	fmt.Println(arg1)
	return []string{"1","2","3","4"}
}

func TransactionAppliedDiscount(context *lex) float64 {
	return float64(10)
}

func TransactionProducts(context *lex,arg1, arg2, arg3 float64,args []string) ([]ProductResult, error) {
	fmt.Println(arg1, arg2, arg3, args)
	return []ProductResult{
		{Quantity:1,Amount:34,ProductID: "ID1"},
		{Quantity:2,Amount:34,ProductID: "ID2"},
	}, nil
}

func Contains(x string, a []string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
