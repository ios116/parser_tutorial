package newformula

type CounterObj struct {
	CounterCode             string  `json:"counter_code" yaml:"counter_code" `
	AccountID               string  `json:"account_id" yaml:"account_id"`
	BusinessRuleID          string  `json:"business_rule_id" yaml:"business_rule_id"`
	Value                   float64 `json:"value" yaml:"value"`
	Name                    string  `json:"name" yaml:"name"`
	Cyclic                  bool    `json:"cyclic" yaml:"cyclic"`
	IncrementFormula        string  `json:"inc_formula" yaml:"inc_formula"`
	Limit                   float64 `json:"limit" yaml:"limit"`
	AccumulationPeriodValue int     `json:"acc_period_value" yaml:"acc_period_value"`
	AccumulationPeriodType  int     `json:"acc_period_type" yaml:"acc_period_type"`
	TTL                     string  `json:"ttl" yaml:"ttl"`
}


// ProductResult структура возвращается из метода Products.
type ProductResult struct {
	Quantity  float64 `json:"quantity" yaml:"quantity"`
	Amount    float64 `json:"amount" yaml:"amount"`
	ProductID string  `json:"product_id" yaml:"product_id"`
}

type BusinessRule struct {
	Description string `json:"description" yaml:"description"`
}
