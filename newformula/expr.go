package newformula

import (
	"github.com/antonmedv/expr"
)


type Transaction2 struct {
	Account     AccountItem  `json:"account_item" yaml:"account_item"`
	TriggeredDiscount int
	AppliedDiscount int
}

// AccountItem структура возвращается из метода Account.
type AccountItem struct {

}

// Counter счетчики.
// проверяем значение счетчика и время экспирации ttl.
func (p AccountItem) Counter(ruleID string, counterCode string) CounterObj {

	return CounterObj{Value:34}
}

func witExpr(data string) interface{} {
	env := map[string]interface{}{
		"Transaction": Transaction2{TriggeredDiscount: 34 },
	}
	out, err := expr.Eval(data, env)
	if err != nil {
		panic(err)
	}

	return out
}