package newformula

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"unicode"
	"unicode/utf8"
)

//go:generate go run golang.org/x/tools/cmd/goyacc -l -o parser.go parser.y

type lex struct {
	input   []byte
	result  interface{}
	err     error
	pos     int
	width   int
	context *ResultActions
}

func newLex(input []byte, context *ResultActions) *lex {
	return &lex{
		input:   input,
		context: context,
	}
}

// Parse parses the input and returs the result.
func Parse(context *ResultActions, input []byte) (interface{}, error) {
	l := newLex(input, context)
	r := yyParse(l)
	if r !=-1 {

	}
	return l.result, l.err
}

// Lex satisfies yyLexer.
func (l *lex) Lex(lval *yySymType) int {
	return l.scanNormal(lval)
}

func (l *lex) scanNormal(lval *yySymType) int {
	for b := l.next(); b != 0; b = l.next() {
		switch {
		case b == '\'' || b == '"':
			return l.scanArg(lval)
		case unicode.IsSpace(b):
			continue
		case unicode.IsNumber(b):
			return l.scanFloat(lval)
		case unicode.IsLetter(b):
			return l.scanString(lval)
		default:
			return int(b)
		}
	}
	return 0
}

func (l *lex) scanArg(lval *yySymType) int {
	buf := bytes.NewBuffer(nil)
	for b := l.next(); b != 0; b = l.next() {
		switch b {
		case '\'':
			lval.string = buf.String()
			return Arg
		case '"':
			lval.string = buf.String()
			return Arg
		default:
			buf.WriteRune(b)
		}
	}
	return LexError
}

func (l *lex) scanFloat(lval *yySymType) int {
	l.backup()
	buf := bytes.NewBuffer(nil)
	GOTO:
	for b := l.next(); b != 0; b = l.next() {
		switch {
		case unicode.IsLetter(b):
			return 0
		case !unicode.IsNumber(b) && b != '.':
			break GOTO
		default:
			//if b == '.' {
			//	intager= false
			//}
			buf.WriteRune(b)
		}
	}

	str := buf.String()
	//if intager {
	//	return
	//}
	val, _ := strconv.ParseFloat(str, 64)
	lval.double = val
	l.backup()
	return FLOAT
}

func (l *lex) scanString(lval *yySymType) int {
	l.backup()
	buf := bytes.NewBuffer(nil)
	defer func() {
		lval.string = buf.String()
	}()
	GOTO:
	for b := l.next(); b != 0; b = l.next() {
		if b != '_' && !unicode.IsLetter(b) {
			break GOTO
		}
		buf.WriteRune(b)
	}
	l.backup()
	switch buf.String() {
	case "Transaction":
		return Transaction
	case "Account":
		return Account
	case "Counter":
		return Counter
	case "Value":
		return Value
	case "IncrementValue":
		return IncrementValue
	case "CustomAttribute":
		return CustomAttribute
	case "Customer":
		return Customer
	case "TriggeredDiscount":
		return TriggeredDiscount
	case "AppliedDiscount":
		return AppliedDiscount
	case "TTL":
		return TTL
	case "Limit":
		return Limit
	case "Difference":
		return Difference
	case "AND":
		return AND
	case "OR":
		return OR
	case "Products":
		return Products
	case "in":
		return in
	case "Rule":
		return Rule
	case "Description":
		return Description
	}
	return 0
}

// Error satisfies yyLexer.
func (l *lex) Error(s string) {
	l.err = errors.New(fmt.Sprintf("expresion: [%s] in position %d %s",string(l.input),l.pos,s))
}

func (l *lex) next() rune {
	if l.pos >= len(l.input) || l.pos == -1 {
		l.pos = -1
		return 0
	}
	r, s := utf8.DecodeRune(l.input[l.pos:])
	l.width = s
	l.pos += l.width

	return r
}

func (l *lex) backup() {
	if l.pos == -1 {
		return
	}
	l.pos -= l.width
}
