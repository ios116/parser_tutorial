%{
package newformula
func setResult(l yyLexer, v interface{}) {
  l.(*lex).result = v
}
func yyerror(l yyLexer ,err error) {
	if err != nil {l.Error(err.Error())}
}
%}

%union{
string string
bool bool
char rune
any interface{}
cnt CounterObj
productResults []ProductResult
businessRule BusinessRule
double float64
intager int
strArr []string
intArr []int
}

%left AND OR
%left '>' '<' GE LE EQ NE
%left '+' '-'
%left '*' '/' '%'

%token LexError

%token <string> Transaction Account Counter Arg Customer TTL AND OR Products in Rule Description
%token <any> TriggeredDiscount AppliedDiscount
%token <double> Value Limit FLOAT IncrementValue Difference
%token <intager> NUMBER
%token <strArr> CustomAttribute

%type <businessRule> rule
%type <productResults> poroducts
%type <strArr> args array
%type <cnt> counter
%type <any> expression result
%type <bool> comparison logical bolean
%type <double> float math
%type <string> string
%start main

%%

main: result
 {
    setResult(yylex,$1)
 }
result:
expression { $$ = $1 }
| math { $$ = $1 }
| logical { $$ = $1; }
| bolean { $$ = $1}
| string { $$= $1}

// базове математческие опреаци
math:
float { $$ = $1 }
| math '+' math { $$ = $1 + $3 }
| math '-' math { $$ = $1 - $3 }
| math '*' math { $$ = $1 * $3 }
| math '/' math { $$ = $1 / $3 }
| '(' math ')' { $$ = $2; }

// Сравнения
logical: comparison { $$ = $1 }
| logical AND comparison  { $$ = $1 && $3 }
| logical OR comparison { $$ = $1 || $3 }
| '(' logical ')' { $$ = $2; }

comparison:
 math '>' math { $$ = $1 > $3}
 | math '<' math { $$ = $1 < $3 }
 
// массивы
array: '[' args ']' { $$ = $2 }

args:
string {  $$ = []string{$1}}
| args ',' string { $$ = append( $1 , $3 ) }

// Формулы
expression: customer '.' CustomAttribute '(' string ')' { $$=TransactionCustomerCustomAttribute(yylex.(*lex),$5)}
| poroducts { $$ = $1 }
| array { $$ = $1 }

string:  Arg { $$ = $1 }
| rule '.' Description {$$= $1.Description}
| counter '.' TTL { $$ = $1.TTL }

bolean:
  Arg in array { $$= Contains($1,$3) }

float:
FLOAT { $$ = $1 }
| Transaction '.' AppliedDiscount { $$=TransactionAppliedDiscount(yylex.(*lex))}
| counter '.' Value { $$ = $1.Value }
| counter '.' IncrementValue { $$ = $1.Value }
| counter '.' Difference { $$ = $1.Limit - $1.Value }
| counter '.' Limit {$$ = $1.Limit }

rule: Transaction '.' Rule '(' string ')' { $$= TransactionRule(yylex.(*lex),$5)}
counter: Transaction '.' Account '(' ')' '.' Counter '(' string ',' string ')' {$$ = TransactionAccountCounter(yylex.(*lex),$9,$11) }
customer: Transaction '.' Customer '(' ')'

poroducts:
Transaction '.' Products '(' float ',' float ',' float ',' args ')' {
	var err error
	$$, _ = TransactionProducts(yylex.(*lex),$5,$7,$9,$11)
	yyerror(yylex,err)
}
| Transaction '.' Products '(' float ',' float ',' float ')' {
	var err error
	$$, err = TransactionProducts(yylex.(*lex),$5,$7,$9,nil)
	yyerror(yylex,err)
}



