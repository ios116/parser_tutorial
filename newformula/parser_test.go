package newformula

import (
	"testing"
	"unicode"
)

func TestYaccParser(t *testing.T) {
	testCases := []struct {
		name     string
		str      string
		wantErr  error
		expected interface{}
	}{
		{
			name: "args",
			str:  `Transaction.Products(1,2,3,'arg1','arg2','arg3')`,
		},
		{
			name: "args",
			str:  `Transaction.Products(1,2,Transaction.Account().Counter('rule1','counter234').Value)`,
		},
		{
			name: "args",
			str:  ` Transaction.Customer().CustomAttribute('hello')`,
		},
		{
			name: "args with error",
			str:  `Transaction.Products(1,2,3)`,
		},
		{
			name: "array",
			str:  `["CN","CN2","CN3"]`,
		},
		{
			name: "one",
			str:  "Transaction.Rule('rule_code').Description",
		},
		{
			name: "brackets",
			str:  "10 / (1+1)",
		},
		{
			name: "brackets and comparison",
			str:  "10 > (1+1)",
		},
		{
			name: "comparison",
			str:  `Transaction.Account().Counter('rule1','counter234').Value > 2 AND 6>2`,
		},
		{
			name: "comparison",
			str:  `Transaction.AppliedDiscount`,
		},
		{
			name: "string in array",
			str:  `"1" in ["1","2"]`,
		},
	}
	for _, tCase := range testCases {
		tCase := tCase
		t.Run(tCase.name, func(t *testing.T) {
			data := &ResultActions{36}
			r, err := Parse(data, []byte(tCase.str))
			if err != nil {
				t.Fatal(err)
			}
			t.Log(r)
		})
	}

}

func TestParserParse2(t *testing.T) {

	if unicode.IsSymbol('|') {
		t.Log("YES")
	}
}

func BenchmarkParserYacc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		str := `Transaction.Account().Counter('rule1','counter234').Value +  Transaction.AppliedDiscount + (2+1)*2`
		data := &ResultActions{36}
		_, err := Parse(data, []byte(str))
		if err != nil {
			b.Fatal(err)
		}
	}
}
func BenchmarkParserExpr(b *testing.B) {
	for i := 0; i < b.N; i++ {
		witExpr("Transaction.Account.Counter('rule1','counter234').Value + Transaction.AppliedDiscount + (2+1)*2")
	}
}

func TestParserParse(t *testing.T) {
	str := `Transaction.Account().Counter('rule1','counter234').Value +(1+3)*2 `
	for i := 0; i < 10; i++ {
		t.Run("1", func(t *testing.T) {
			t.Parallel()
			data := &ResultActions{i}
			r, err := Parse(data, []byte(str))
			if err != nil {
				t.Fatal(err)
			}
			t.Log(r)
		})
	}
}
